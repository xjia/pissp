#ifndef PISSP_TOKENS_H_
#define PISSP_TOKENS_H_

enum {
	T_EOF = 0,
	
	K_IF,
	K_ELIF,
	K_ELSE,
	K_END,
	K_WHILE,
	K_DO_WHILE,
	K_FOR,
	K_TO,
	K_BY,
	K_AS,
	K_IN,
	K_DEF,
	K_BREAK,
	K_CONTINUE,
	K_RETURN,
	K_OR,
	K_XOR,
	K_AND,
	K_INSTANCEOF,
	
	T_ASSIGN,   // =
	T_MAPSTO,   // =>
	T_LPAREN,   // (
	T_RPAREN,   // )
	T_COMMA,    // ,
	T_AND,      // &
	T_OR,       // |
	T_XOR,      // ^
	T_ADD_ASGN, // +=
	T_SUB_ASGN, // -=
	T_MUL_ASGN, // *=
	T_DIV_ASGN, // /=
	T_MOD_ASGN, // %=
	T_DOT_ASGN, // .=
	T_AND_ASGN, // &=
	T_OR_ASGN,  // |=
	T_XOR_ASGN, // ^=
	T_SHL_ASGN, // <<=
	T_SHR_ASGN, // >>=
	T_LBRACKET, // [
	T_RBRACKET, // ]
	T_QUESMARK, // ?
	T_COLON,    // :
	T_LOR,      // ||
	T_LAND,     // &&
	T_EQ,       // ==
	T_NEQ,      // !=  <>
	T_EQUIV,    // ===
	T_NEQUIV,   // !==
	T_LT,       // <
	T_LE,       // <=
	T_GT,       // >
	T_GE,       // >=
	T_DIV,      // /
	T_SHL,      // <<
	T_SHR,      // >>
	T_ADD,      // +
	T_SUB,      // -
	T_DOT,      // .
	T_MOD,      // %
	T_MUL,      // *
	T_ADDADD,   // ++
	T_SUBSUB,   // --
	T_NOT,      // !
	T_LBRACE,   // {
	T_RBRACE,   // }
	
	T_HEREDOC,
	T_SINGLE_QUOTED,
	T_DOUBLE_QUOTED,
	
	K_TRUE,
	K_FALSE,
	K_NULL,
	
	T_NUMBER,
	T_NAME,
	
	T_HTML,
	T_HTML_ECHO,
	// TODO interpolation
};

#endif