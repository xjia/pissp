#include "buffer.h"
#include <stdlib.h>
#include <string.h>

struct buffer_t {
	char* data;
	size_t length;
	size_t capacity;
};

buffer_p buffer_new(const char* initial_value) {
	struct buffer_t* b;
	
	b = malloc(sizeof(struct buffer_t));
	b->length = strlen(initial_value);
	b->capacity = b->length + 1;
	b->data = malloc(sizeof(char) * b->capacity);
	strcpy(b->data, initial_value);
	return b;
}

void buffer_free(buffer_p v) {
	if (v) {
		struct buffer_t* b;
		
		b = v;
		free(b->data);
		free(b);
	}
}

int buffer_is_empty(buffer_p v) {
	struct buffer_t* b;
	
	b = v;
	return b->length == 0;
}

char buffer_pop_back(buffer_p v) {
	char c;
	struct buffer_t* b;
	
	b = v;
	b->length--;
	c = b->data[b->length];
	b->data[b->length] = '\0';
	return c;
}

static int is_full(struct buffer_t* b) {
	// In reality it should never exceed buffer capacity
	return b->length + 1 >= b->capacity;
}

static void double_capacity(struct buffer_t* b) {
	char* old;
	
	old = b->data;
	b->capacity *= 2;
	b->data = malloc(sizeof(char) * b->capacity);
	strcpy(b->data, old);
	free(old);
}

void buffer_push_back(buffer_p v, char c) {
	struct buffer_t* b;
	
	b = v;
	if (is_full(b)) {
		double_capacity(b);
	}
	b->data[b->length++] = c;
	b->data[b->length] = '\0';
}

void buffer_clear(buffer_p v) {
	struct buffer_t* b;
	
	b = v;
	b->length = 0;
	b->data[0] = '\0';
}

const char* buffer_get_raw(buffer_p v) {
	struct buffer_t* b;
	
	b = v;
	return b->data;
}