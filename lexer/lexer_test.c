#include <stdio.h>
#include <string.h>
#include "lexer.h"

int main(int argc, char** argv) {
	lexer_p l;
	int tok;

	l = lexer_new(stdin, stderr);
	while ((tok = lexer_next_token(l))) {
		printf("%s %u\n", lexer_to_string(tok), (unsigned) strlen(lexer_get_buffer(l)));
	}
	lexer_free(l);
	return 0;
}