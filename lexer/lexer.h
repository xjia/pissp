#ifndef PISSP_LEXER_H_
#define PISSP_LEXER_H_

#include <stdio.h>

typedef void* lexer_p;

extern lexer_p lexer_new(FILE* input, FILE* error);
extern void lexer_free(lexer_p lexer);
extern int lexer_next_token(lexer_p lexer);
extern const char* lexer_to_string(const int token);
extern const char* lexer_get_buffer(lexer_p lexer);

#endif