#ifndef PISSP_BUFFER_H_
#define PISSP_BUFFER_H_

typedef void* buffer_p;

extern buffer_p buffer_new(const char* initial_value);
extern void buffer_free(buffer_p buffer);
extern int buffer_is_empty(buffer_p buffer);
extern char buffer_pop_back(buffer_p buffer);
extern void buffer_push_back(buffer_p buffer, char ch);
extern void buffer_clear(buffer_p buffer);
extern const char* buffer_get_raw(buffer_p buffer);

#endif