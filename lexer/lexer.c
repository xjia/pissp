#include "lexer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "buffer.h"

enum {
	STATE_IN_CODE,
	STATE_IN_STRING,
	STATE_IN_HTML,
	STATE_END_OF_FILE,
};

#include "tokens.h"

struct lexer_t {
	int state;
	FILE* input_file;
	FILE* error_file;
	buffer_p input_buffer;
	buffer_p token_buffer;
};

static int get_char(struct lexer_t* l) {
	if (buffer_is_empty(l->input_buffer)) {
		return fgetc(l->input_file);
	} else {
		return buffer_pop_back(l->input_buffer);
	}
}

static void put_back(struct lexer_t* l, int c) {
	buffer_push_back(l->input_buffer, c);
}

static void init_buffer(struct lexer_t* l) {
	buffer_clear(l->token_buffer);
}

static void append_buffer(struct lexer_t* l, int c) {
	buffer_push_back(l->token_buffer, c);
}

const char* lexer_get_buffer(lexer_p v) {
	struct lexer_t* l;
	
	l = v;
	return buffer_get_raw(l->token_buffer);
}

static int is_blank_char(int c) {
	return ' ' == c || '\t' == c;
}

static int is_digit_char(int c) {
	return '0' <= c && c <= '9';
}

static int is_escape_char(int c) {
	switch (c) {
		case 'n':
		case 'r':
		case 't':
		case 'v':
		case 'e':
		case 'f':
			return 1;
	}
	return 0;
}

static int escape_char(int c) {
	switch (c) {
		case 'n': return '\n';
		case 'r': return '\r';
		case 't': return '\t';
		case 'v': return '\v';
		case 'e': return '\e';
		case 'f': return '\f';
	}
	return 0;
}

static int is_eol_char(int c) {
	return '\r' == c || '\n' == c;
}

static int is_eof_char(int c) {
	return -1 == c;
}

static int is_lower_char(int c) {
	return 'a' <= c && c <= 'z';
}

static int is_upper_char(int c) {
	return 'A' <= c && c <= 'Z';
}

static int is_alpha_char(int c) {
	return '_' == c || is_lower_char(c) || is_upper_char(c);
}

static int is_name_first_char(int c) {
	return '$' == c || is_alpha_char(c);
}

static int is_name_char(int c) {
	return is_alpha_char(c) || is_digit_char(c);
}

lexer_p lexer_new(FILE* in, FILE* err) {
	struct lexer_t* l;
	
	l = malloc(sizeof(struct lexer_t));
	l->state = STATE_IN_HTML;
	l->input_file = in;
	l->error_file = err;
	l->input_buffer = buffer_new("");
	l->token_buffer = buffer_new("");
	return l;
}

void lexer_free(lexer_p v) {
	if (v) {
		struct lexer_t* l;
		
		l = v;
		buffer_free(l->input_buffer);
		buffer_free(l->token_buffer);
		free(l);
	}
}

int lexer_next_token(lexer_p v) {
	struct lexer_t* l;
	int c;
	
	l = v;
	init_buffer(l);
next_token:
	c = get_char(l);
	if (STATE_END_OF_FILE == l->state) {
		return T_EOF;
	} else if (STATE_IN_CODE == l->state || STATE_IN_STRING == l->state) {
		while (is_blank_char(c) || is_eol_char(c)) {
			c = get_char(l);
		}
		if (is_eof_char(c)) {
			l->state = STATE_END_OF_FILE;
			return T_EOF;
		} else if ('#' == c) {
comment_1:
			c = get_char(l);
			if (is_eol_char(c)) {
				goto next_token;
			} else if (is_eof_char(c)) {
				l->state = STATE_END_OF_FILE;
				return T_EOF;
			} else if ('%' == c) {
				c = get_char(l);
				if ('>' == c) {
					put_back(l, '>');
					put_back(l, '%');
					goto next_token;
				} else {
					put_back(l, c);
					goto comment_1;
				}
			} else {
				goto comment_1;
			}
		} else if ('!' == c) {
			c = get_char(l);
			if ('=' == c) {
				c = get_char(l);
				if ('=' == c) {
					return T_NEQUIV;
				} else {
					put_back(l, c);
					return T_NEQ;
				}
			} else {
				put_back(l, c);
				return T_NOT;
			}
		} else if ('%' == c) {
			c = get_char(l);
			if ('=' == c) {
				return T_MOD_ASGN;
			} else if ('>' == c) {
				l->state = STATE_IN_HTML;
state_in_html_1:
				c = get_char(l);
				if (is_eof_char(c)) {
					l->state = STATE_END_OF_FILE;
					return T_HTML;
				} else if ('<' == c) {
					c = get_char(l);
					if ('%' == c) {
						l->state = STATE_IN_CODE;
						c = get_char(l);
						if ('=' == c) {
							return T_HTML_ECHO;
						} else {
							put_back(l, c);
							return T_HTML;
						}
					} else {
						put_back(l, c);
						goto state_in_html_1;
					}
				} else {
					append_buffer(l, c);
					goto state_in_html_1;
				}
			} else {
				put_back(l, c);
				return T_MOD;
			}
		} else if ('&' == c) {
			c = get_char(l);
			if ('&' == c) {
				return T_LAND;
			} else if ('=' == c) {
				return T_AND_ASGN;
			} else {
				put_back(l, c);
				return T_AND;
			}
		} else if ('(' == c) {
			return T_LPAREN;
		} else if (')' == c) {
			return T_RPAREN;
		} else if ('*' == c) {
			c = get_char(l);
			if ('=' == c) {
				return T_MUL_ASGN;
			} else {
				put_back(l, c);
				return T_MUL;
			}
		} else if ('+' == c) {
			c = get_char(l);
			if ('+' == c) {
				return T_ADDADD;
			} else if ('=' == c) {
				return T_ADD_ASGN;
			} else {
				put_back(l, c);
				return T_ADD;
			}
		} else if (',' == c) {
			return T_COMMA;
		} else if ('-' == c) {
			c = get_char(l);
			if ('-' == c) {
				return T_SUBSUB;
			} else if ('=' == c) {
				return T_SUB_ASGN;
			} else {
				put_back(l, c);
				return T_SUB;
			}
		} else if ('.' == c) {
			c = get_char(l);
			if ('=' == c) {
				return T_DOT_ASGN;
			} else if (is_digit_char(c)) {
				while (is_digit_char(c)) {
					append_buffer(l, c);
					c = get_char(l);
				}
				put_back(l, c);
				return T_NUMBER;
			} else {
				put_back(l, c);
				return T_DOT;
			}
		} else if ('/' == c) {
			c = get_char(l);
			if ('/' == c) {
comment_2:
				c = get_char(l);
				if (is_eol_char(c)) {
					goto next_token;
				} else if (is_eof_char(c)) {
					l->state = STATE_END_OF_FILE;
					return T_EOF;
				} else if ('%' == c) {
					c = get_char(l);
					if ('>' == c) {
						put_back(l, '>');
						put_back(l, '%');
						goto next_token;
					} else {
						put_back(l, c);
						goto comment_2;
					}
				} else {
					goto comment_2;
				}
			} else if ('=' == c) {
				return T_DIV_ASGN;
			} else {
				put_back(l, c);
				return T_DIV;
			}
		} else if (':' == c) {
			return T_COLON;
		} else if ('<' == c) {
			c = get_char(l);
			if ('<' == c) {
				c = get_char(l);
				if ('=' == c) {
					return T_SHL_ASGN;
				} else {
					put_back(l, c);
					return T_SHL;
				}
			} else if ('=' == c) {
				return T_LE;
			} else if ('>' == c) {
				return T_NEQ;
			} else {
				put_back(l, c);
				return T_LT;
			}
		} else if ('=' == c) {
			c = get_char(l);
			if ('=' == c) {
				c = get_char(l);
				if ('=' == c) {
					return T_EQUIV;
				} else {
					put_back(l, c);
					return T_EQ;
				}
			} else if ('>' == c) {
				return T_MAPSTO;
			} else {
				put_back(l, c);
				return T_ASSIGN;
			}
		} else if ('>' == c) {
			c = get_char(l);
			if ('=' == c) {
				return T_GE;
			} else if ('>' == c) {
				c = get_char(l);
				if ('=' == c) {
					return T_SHR_ASGN;
				} else {
					put_back(l, c);
					return T_SHR;
				}
			} else {
				put_back(l, c);
				return T_GT;
			}
		} else if ('?' == c) {
			return T_QUESMARK;
		} else if ('[' == c) {
			return T_LBRACKET;
		} else if (']' == c) {
			return T_RBRACKET;
		} else if ('^' == c) {
			c = get_char(l);
			if ('=' == c) {
				return T_XOR_ASGN;
			} else {
				put_back(l, c);
				return T_XOR;
			}
		} else if ('{' == c) {
			return T_LBRACE;
		} else if ('|' == c) {
			c = get_char(l);
			if ('=' == c) {
				return T_OR_ASGN;
			} else if ('|' == c) {
				return T_LOR;
			} else {
				put_back(l, c);
				return T_OR;
			}
		} else if ('}' == c) {
			if (STATE_IN_STRING == l->state) {
				// TODO check nesting level
			} else {
				return T_RBRACE;
			}
		} else if ('"' == c) {
			if (STATE_IN_STRING == l->state) {
				fputs("ERROR: double quoted string not allowed in interpolation state", l->error_file);
				l->state = STATE_END_OF_FILE;
				return T_EOF;
			} else {
				c = get_char(l);
				if ('"' == c) {
					c = get_char(l);
					if ('"' == c) {
heredoc_1:
						c = get_char(l);
						if ('"' == c) {
							c = get_char(l);
							if ('"' == c) {
								c = get_char(l);
								if ('"' == c) {
									return T_HEREDOC;
								} else {
									append_buffer(l, '"');
									append_buffer(l, '"');
									append_buffer(l, c);
									goto heredoc_1;
								}
							} else {
								append_buffer(l, '"');
								append_buffer(l, c);
								goto heredoc_1;
							}
						} else {
							append_buffer(l, c);
							goto heredoc_1;
						}
					} else {
						put_back(l, c);
						return T_DOUBLE_QUOTED;
					}
				} else if ('\\' == c) {
double_quoted_2:
					c = get_char(l);
					if (is_escape_char(c)) {
						append_buffer(l, escape_char(c));
					} else {
						append_buffer(l, c);
					}
double_quoted_1:
					c = get_char(l);
					if ('"' == c) {
						return T_DOUBLE_QUOTED;
					} else if ('\\' == c) {
						goto double_quoted_2;
					} else if (is_eol_char(c)) {
						fputs("ERROR: new line not allowed in double quoted string", l->error_file);
						l->state = STATE_END_OF_FILE;
						return T_EOF;
					} else {
						append_buffer(l, c);
						goto double_quoted_1;
					}
				} else if (is_eol_char(c)) {
					fputs("ERROR: new line not allowed in double quoted string", l->error_file);
					l->state = STATE_END_OF_FILE;
					return T_EOF;
				} else {
					append_buffer(l, c);
					goto double_quoted_1;
				}
			}
		} else if ('\'' == c) {
			c = get_char(l);
			if ('\'' == c) {
				return T_SINGLE_QUOTED;
			} else if ('\\' == c) {
single_quoted_2:
				c = get_char(l);
				if ('\\' == c || '\'' == c) {
					append_buffer(l, c);
				} else {
					fputs("ERROR: escape chars are not available in single quoted string", l->error_file);
					l->state = STATE_END_OF_FILE;
					return T_EOF;
				}
single_quoted_1:
				c = get_char(l);
				if ('\\' == c) {
					goto single_quoted_2;
				} else if (is_eol_char(c)) {
					fputs("ERROR: new line now allowed in single quoted string", l->error_file);
					l->state = STATE_END_OF_FILE;
					return T_EOF;
				} else if ('\'' == c) {
					return T_SINGLE_QUOTED;
				} else {
					append_buffer(l, c);
					goto single_quoted_1;
				}
			} else if (is_eol_char(c)) {
				fputs("ERROR: new line now allowed in single quoted string", l->error_file);
				l->state = STATE_END_OF_FILE;
				return T_EOF;
			} else {
				append_buffer(l, c);
				goto single_quoted_1;
			}
		} else if (is_digit_char(c)) {
			if ('0' == c) {
				append_buffer(l, c);
				c = get_char(l);
				if ('x' == c) {
					c = get_char(l);
					if (is_digit_char(c)) {
						append_buffer(l, 'x');
						while (is_digit_char(c)) {
							append_buffer(l, c);
							c = get_char(l);
						}
						put_back(l, c);
						return T_NUMBER;
					} else {
						fputs("ERROR: only 0x detected; digitals expected", l->error_file);
						l->state = STATE_END_OF_FILE;
						return T_EOF;
					}
				} else if ('.' == c) {
					goto number_2;
				} else if (is_digit_char(c)) {
					append_buffer(l, c);
					goto number_1;
				} else {
					put_back(l, c);
					return T_NUMBER;
				}
			} else {
				// 1-9
				append_buffer(l, c);
number_1:
				c = get_char(l);
				if ('.' == c) {
number_2:
					c = get_char(l);
					if (is_digit_char(c)) {
						append_buffer(l, '.');
						while (is_digit_char(c)) {
							append_buffer(l, c);
							c = get_char(l);
						}
						put_back(l, c);
						return T_NUMBER;
					} else {
						put_back(l, c);
						put_back(l, '.');
						return T_NUMBER;
					}
				} else if (is_digit_char(c)) {
					append_buffer(l, c);
					goto number_1;
				} else {
					put_back(l, c);
					return T_NUMBER;
				}
			}
		} else if (is_name_first_char(c)) {
			append_buffer(l, c);
			while (is_name_char(c = get_char(l))) {
				append_buffer(l, c);
			}
			put_back(l, c);
			if (!strcmp("true", lexer_get_buffer(l))) {
				return K_TRUE;
			} else if (!strcmp("false", lexer_get_buffer(l))) {
				return K_FALSE;
			} else if (!strcmp("null", lexer_get_buffer(l))) {
				return K_NULL;
			} else if (!strcmp("if", lexer_get_buffer(l))) {
				return K_IF;
			} else if (!strcmp("elif", lexer_get_buffer(l))) {
				return K_ELIF;
			} else if (!strcmp("else", lexer_get_buffer(l))) {
				return K_ELSE;
			} else if (!strcmp("end", lexer_get_buffer(l))) {
				return K_END;
			} else if (!strcmp("while", lexer_get_buffer(l))) {
				return K_WHILE;
			} else if (!strcmp("do_while", lexer_get_buffer(l))) {
				return K_DO_WHILE;
			} else if (!strcmp("for", lexer_get_buffer(l))) {
				return K_FOR;
			} else if (!strcmp("to", lexer_get_buffer(l))) {
				return K_TO;
			} else if (!strcmp("by", lexer_get_buffer(l))) {
				return K_BY;
			} else if (!strcmp("as", lexer_get_buffer(l))) {
				return K_AS;
			} else if (!strcmp("in", lexer_get_buffer(l))) {
				return K_IN;
			} else if (!strcmp("def", lexer_get_buffer(l))) {
				return K_DEF;
			} else if (!strcmp("break", lexer_get_buffer(l))) {
				return K_BREAK;
			} else if (!strcmp("continue", lexer_get_buffer(l))) {
				return K_CONTINUE;
			} else if (!strcmp("return", lexer_get_buffer(l))) {
				return K_RETURN;
			} else if (!strcmp("or", lexer_get_buffer(l))) {
				return K_OR;
			} else if (!strcmp("xor", lexer_get_buffer(l))) {
				return K_XOR;
			} else if (!strcmp("and", lexer_get_buffer(l))) {
				return K_AND;
			} else if (!strcmp("instanceof", lexer_get_buffer(l))) {
				return K_INSTANCEOF;
			} else {
				return T_NAME;
			}
		} else {
			fprintf(l->error_file, "ERROR: illegal character `%c'\n", c);
			l->state = STATE_END_OF_FILE;
			return T_EOF;
		}
	} else if (STATE_IN_HTML == l->state) {
		put_back(l, c);
		goto state_in_html_1;
	} else {
		fprintf(l->error_file, "ERROR: invalid lexer state %d\n", l->state);
	}	
	l->state = STATE_END_OF_FILE;
	return T_EOF;
}

const char* lexer_to_string(const int tok) {
	switch (tok) {
		case T_EOF: return "EOF";
		case K_IF: return "if";
		case K_ELIF: return "elif";
		case K_ELSE: return "else";
		case K_END: return "end";
		case K_WHILE: return "while";
		case K_DO_WHILE: return "do_while";
		case K_FOR: return "for";
		case K_TO: return "to";
		case K_BY: return "by";
		case K_AS: return "as";
		case K_IN: return "in";
		case K_DEF: return "def";
		case K_BREAK: return "break";
		case K_CONTINUE: return "continue";
		case K_RETURN: return "return";
		case K_OR: return "or";
		case K_XOR: return "xor";
		case K_AND: return "and";
		case K_INSTANCEOF: return "instanceof";
		case T_ASSIGN: return "=";
		case T_MAPSTO: return "=>";
		case T_LPAREN: return "(";
		case T_RPAREN: return ")";
		case T_COMMA: return ",";
		case T_AND: return "&";
		case T_OR: return "|";
		case T_XOR: return "^";
		case T_ADD_ASGN: return "+=";
		case T_SUB_ASGN: return "-=";
		case T_MUL_ASGN: return "*=";
		case T_DIV_ASGN: return "/=";
		case T_MOD_ASGN: return "%=";
		case T_DOT_ASGN: return ".=";
		case T_AND_ASGN: return "&=";
		case T_OR_ASGN: return "|=";
		case T_XOR_ASGN: return "^=";
		case T_SHL_ASGN: return "<<=";
		case T_SHR_ASGN: return ">>=";
		case T_LBRACKET: return "[";
		case T_RBRACKET: return "]";
		case T_QUESMARK: return "?";
		case T_COLON: return ":";
		case T_LOR: return "||";
		case T_LAND: return "&&";
		case T_EQ: return "==";
		case T_NEQ: return "!=";
		case T_EQUIV: return "===";
		case T_NEQUIV: return "!==";
		case T_LT: return "<";
		case T_LE: return "<=";
		case T_GT: return ">";
		case T_GE: return ">=";
		case T_DIV: return "/";
		case T_SHL: return "<<";
		case T_SHR: return ">>";
		case T_ADD: return "+";
		case T_SUB: return "-";
		case T_DOT: return ".";
		case T_MOD: return "%";
		case T_MUL: return "*";
		case T_ADDADD: return "++";
		case T_SUBSUB: return "--";
		case T_NOT: return "!";
		case T_LBRACE: return "{";
		case T_RBRACE: return "}";
		case T_HEREDOC: return "HEREDOC";
		case T_SINGLE_QUOTED: return "SINGLE_QUOTED";
		case T_DOUBLE_QUOTED: return "DOUBLE_QUOTED";
		case K_TRUE: return "true";
		case K_FALSE: return "false";
		case K_NULL: return "null";
		case T_NUMBER: return "NUMBER";
		case T_NAME: return "NAME";
		case T_HTML: return "HTML";
		case T_HTML_ECHO: return "HTML_ECHO";
	}
	return "<?>";
}