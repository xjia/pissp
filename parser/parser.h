#ifndef PISSP_PARSER_H_
#define PISSP_PARSER_H_

#include <stdio.h>
#include "node.h"

typedef void* parser_p;

extern parser_p parser_new(FILE* input, FILE* error);
extern void parser_free(parser_p parser);
extern node_p parser_parse_program(parser_p parser);
extern node_p parser_parse_block(parser_p parser);
extern node_p parser_parse_statement(parser_p parser);
extern node_p parser_parse_if(parser_p parser);
extern node_p parser_parse_while(parser_p parser);
extern node_p parser_parse_do_while(parser_p parser);
extern node_p parser_parse_for(parser_p parser);
extern node_p parser_parse_def(parser_p parser);
extern node_p parser_parse_def_params(parser_p parser);
extern node_p parser_parse_break(parser_p parser);
extern node_p parser_parse_continue(parser_p parser);
extern node_p parser_parse_return(parser_p parser);
extern node_p parser_parse_assignment(parser_p parser);
extern node_p parser_parse_expression(parser_p parser);

#endif