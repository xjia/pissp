#include "node.h"
#include <stdlib.h>
#include <string.h>
#include "../lexer/buffer.h"

enum {
	INITIAL_CAPACITY = 5
};

struct node_t {
	int type;
	buffer_p text;
	int size;
	int capacity;
	node_p* children;
};

node_p node_new(int type, const char* text) {
	struct node_t* n;
	
	n = malloc(sizeof(struct node_t));
	n->type = type;
	n->text = buffer_new(text);
	n->size = 0;
	n->capacity = INITIAL_CAPACITY;
	n->children = malloc(sizeof(node_p) * n->capacity);
	return n;
}

void node_free(node_p node) {
	struct node_t* n;
	
	n = node;
	buffer_free(n->text);
	free(n->children);
	free(n);
}

void node_free_recursively(node_p node) {
	size_t i;
	
	for (i = 0; i < node_get_size(node); i++) {
		node_free_recursively(node_get_child(node, i));
	}
	node_free(node);
}

int node_get_type(node_p node) {
	struct node_t* n;
	
	n = node;
	return n->type;
}

const char* node_get_text(node_p node) {
	struct node_t* n;
	
	n = node;
	return buffer_get_raw(n->text);
}

size_t node_get_size(node_p node) {
	struct node_t* n;
	
	n = node;
	return n->size;
}

node_p node_get_child(node_p node, size_t child_index) {
	struct node_t* n;
	
	n = node;
	if (child_index >= n->size) {
		// ERROR: index out of bound
		return NULL;
	}
	return n->children[child_index];
}

static int is_full(struct node_t* n) {
	return n->size + 1 >= n->capacity;
}

static void double_capacity(struct node_t* n) {
	node_p* old;
	
	old = n->children;
	n->capacity *= 2;
	n->children = malloc(sizeof(node_p) * n->capacity);
	memcpy(n->children, old, sizeof(node_p) * n->size);
	free(old);
}

void node_add_child(node_p node, node_p child) {
	struct node_t* n;
	
	n = node;
	if (is_full(n)) {
		double_capacity(n);
	}
	n->children[n->size++] = child;
}