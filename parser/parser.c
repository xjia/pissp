#include "parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../lexer/lexer.h"

struct parser_t {
	FILE* error_file;
	lexer_p lexer;
};

#include "../lexer/tokens.h"
#include "node_types.h"

parser_p parser_new(FILE* in, FILE* err) {
	struct parser_t* p;
	
	p = malloc(sizeof(struct parser_t));
	p->error_file = err;
	p->lexer = lexer_new(in, err);
	return p;
}

void parser_free(parser_p v) {
	if (v) {
		struct parser_t* p;
		
		p = v;
		lexer_free(p->lexer);
		free(p);
	}
}

node_p parser_parse_program(parser_p v) {
	return parser_parse_block(v);
}

node_p parser_parse_block(parser_p v) {
	struct parser_t* p;
	node_p block, child;
	
	p = v;
	block = node_new(N_BLOCK, "");
	while ((child = parser_parse_statement(p))) {
		node_add_child(block, child);
	}
	return block;
}

node_p parser_parse_statement(parser_p v) {
	struct parser_t* p;
	node_p statement;
	
	p = v;
	if ((statement = parser_parse_if(p))) {
		return statement;
	}
	if ((statement = parser_parse_while(p))) {
		return statement;
	}
	if ((statement = parser_parse_do_while(p))) {
		return statement;
	}
	if ((statement = parser_parse_for(p))) {
		return statement;
	}
	if ((statement = parser_parse_def(p))) {
		return statement;
	}
	if ((statement = parser_parse_break(p))) {
		return statement;
	}
	if ((statement = parser_parse_continue(p))) {
		return statement;
	}
	if ((statement = parser_parse_return(p))) {
		return statement;
	}
	if ((statement = parser_parse_assignment(p))) {
		return statement;
	}
	if ((statement = parser_parse_expression(p))) {
		return statement;
	}
	return NULL;
}

/**
 * Try to match and eat a token or do nothing if doesn't match.
 */
static int match(struct parser_t* p, const int token) {
	// TODO
	return 0;
}

static const char* get_matched_text() {
	//
	return "";
}

static void unmatch(struct parser_t* p, const int token, const char* text) {
	//
}

static void error(struct parser_t* p, const char* msg) {
	fputs(msg, p->error_file);
}

node_p parser_parse_if(parser_p v) {
	struct parser_t* p;
	
	p = v;
	if (match(p, K_IF)) {
		node_p expression;
		
		if ((expression = parser_parse_expression(p))) {
			node_p block;
			
			if ((block = parser_parse_block(p))) {
				node_p node;
				
				node = node_new(N_IF, "");
				node_add_child(node, expression);
				node_add_child(node, block);
				while (match(p, K_ELIF)) {
					if ((expression == parser_parse_expression(p))) {
						node_add_child(node, expression);
						if ((block == parser_parse_block(p))) {
							node_add_child(node, block);
						} else {
							error(p, "block expected after K_ELIF and expression");
							node_free_recursively(node);
							return NULL;
						}
					} else {
						error(p, "expression expected after K_ELIF");
						node_free_recursively(node);
						return NULL;
					}
				}
				if (match(p, K_ELSE)) {
					if ((block = parser_parse_block(p))) {
						node_add_child(node, block);
					} else {
						error(p, "block expected after K_ELSE");
						node_free_recursively(node);
						return NULL;
					}
				}
				if (match(p, K_END)) {
					return node;
				} else {
					error(p, "K_END expected for K_IF");
					node_free_recursively(node);
					return NULL;
				}
			} else {
				error(p, "block expected after K_IF and expression");
				node_free_recursively(expression);
				return NULL;
			}
		} else {
			error(p, "expression expected after K_IF");
			return NULL;
		}
	}
	return NULL;
}

node_p parser_parse_while(parser_p v) {
	struct parser_t* p;
	
	p = v;
	if (match(p, K_WHILE)) {
		node_p expression;
		
		if ((expression = parser_parse_expression(p))) {
			node_p block;
			
			if ((block = parser_parse_block(p))) {
				node_p node;
				
				node = node_new(N_WHILE, "");
				node_add_child(node, expression);
				node_add_child(node, block);
				if (match(p, K_END)) {
					return node;
				} else {
					error(p, "K_END expected for K_WHILE");
					node_free_recursively(node);
					return NULL;
				}
			} else {
				error(p, "block expected after K_WHILE and expression");
				node_free_recursively(expression);
				return NULL;
			}
		} else {
			error(p, "expression expected after K_WHILE");
			return NULL;
		}
	}
	return NULL;
}

node_p parser_parse_do_while(parser_p v) {
	struct parser_t* p;

	p = v;
	if (match(p, K_DO_WHILE)) {
		node_p expression;
	
		if ((expression = parser_parse_expression(p))) {
			node_p block;
		
			if ((block = parser_parse_block(p))) {
				node_p node;
			
				node = node_new(N_DO_WHILE, "");
				node_add_child(node, expression);
				node_add_child(node, block);
				if (match(p, K_END)) {
					return node;
				} else {
					error(p, "K_END expected for K_DO_WHILE");
					node_free_recursively(node);
					return NULL;
				}
			} else {
				error(p, "block expected after K_DO_WHILE and expression");
				node_free_recursively(expression);
				return NULL;
			}
		} else {
			error(p, "expression expected after K_DO_WHILE");
			return NULL;
		}
	}
	return NULL;
}

enum {
	MAX_NAME_LENGTH = 1024
};

node_p parser_parse_for(parser_p v) {
	struct parser_t* p;
	node_p expression;
	char name[2][MAX_NAME_LENGTH];	// XXX This may lead to unknown issues.
	
	p = v;
	if (match(p, K_FOR)) {
		if (match(p, T_NAME)) {
			strcpy(name[0], get_matched_text());
			if (match(p, T_ASSIGN)) {
				if ((expression = parser_parse_expression(p))) {
					node_p node;
					
					node = node_new(N_FOR, "");
					node_add_child(node, node_new(N_NAME, name[0]));
					node_add_child(node, expression);
					if (match(p, K_TO)) {
						if ((expression = parser_parse_expression(p))) {
							node_p block;
							
							node_add_child(node, expression);
							if (match(p, K_BY)) {
								if ((expression = parser_parse_expression(p))) {
									node_add_child(node, expression);
									/* fall through to block */
								} else {
									error(p, "expression expected after K_BY for K_FOR");
									node_free_recursively(node);
									return NULL;
								}
							}
							/* fall through here */
							if ((block = parser_parse_block(p))) {
								node_add_child(node, block);
								if (match(p, K_END)) {
									return node;
								} else {
									error(p, "K_END expected for K_FOR");
									node_free_recursively(node);
									return NULL;
								}
							} else {
								error(p, "block expected for K_FOR");
								node_free_recursively(node);
								return NULL;
							}
						} else {
							error(p, "expression expected after K_TO for K_FOR");
							node_free_recursively(node);
							return NULL;
						}
					} else {
						error(p, "K_TO expected after expression for K_FOR");
						node_free_recursively(node);
						return NULL;
					}
				} else {
					error(p, "expression expected after T_ASSIGN for K_FOR");
					return NULL;
				}
			} else if (match(p, K_IN)) {
				if ((expression = parser_parse_expression(p))) {
					node_p block;
					
					if ((block = parser_parse_block(p))) {
						node_p node;
						
						node = node_new(N_FOR_IN, "");
						node_add_child(node, node_new(N_NAME, name[0]));
						node_add_child(node, expression);
						node_add_child(node, block);
						if (match(p, K_END)) {
							return node;
						} else {
							error(p, "K_END expected for K_FOR");
							node_free_recursively(node);
							return NULL;
						}
					} else {
						error(p, "block expected after K_FOR and expression");
						node_free_recursively(expression);
						return NULL;
					}
				} else {
					error(p, "expression expected for K_FOR");
					return NULL;
				}
			} else {
				/* put back T_NAME and fall through to expression */
				unmatch(p, T_NAME, name[0]);
			}
		}/* end if match T_NAME */
		/* fall through here */
		if ((expression = parser_parse_expression(p))) {
			if (match(p, K_AS)) {
				if (match(p, T_NAME)) {
					strcpy(name[0], get_matched_text());
					if (match(p, T_MAPSTO)) {
						if (match(p, T_NAME)) {
							node_p block;
							
							strcpy(name[1], get_matched_text());
							if ((block = parser_parse_block(p))) {
								node_p node;
								
								node = node_new(N_FOR_AS_TO, "");
								node_add_child(node, expression);
								node_add_child(node, node_new(N_NAME, name[0]));
								node_add_child(node, node_new(N_NAME, name[1]));
								node_add_child(node, block);
								if (match(p, K_END)) {
									return node;
								} else {
									error(p, "K_END expected for K_FOR");
									node_free_recursively(node);
									return NULL;
								}
							} else {
								error(p, "block expected for K_FOR");
								node_free_recursively(expression);
								return NULL;
							}
						} else {
							error(p, "T_NAME expected after T_MAPSTO for K_FOR");
							node_free_recursively(expression);
							return NULL;
						}
					} else {
						node_p block;
						
						if ((block = parser_parse_block(p))) {
							node_p node;
							
							node = node_new(N_FOR_AS, "");
							node_add_child(node, expression);
							node_add_child(node, node_new(N_NAME, name[0]));
							node_add_child(node, block);
							if (match(p, K_END)) {
								return node;
							} else {
								error(p, "K_END expected for K_FOR");
								node_free_recursively(node);
								return NULL;
							}
						} else {
							error(p, "block expected for K_FOR");
							node_free_recursively(expression);
							return NULL;
						}
					}
				} else {
					error(p, "T_NAME expected after K_FOR, expression, and K_AS");
					node_free_recursively(expression);
					return NULL;
				}
			} else {
				error(p, "K_AS expected after K_FOR and expression");
				node_free_recursively(expression);
				return NULL;
			}
		} else {
			error(p, "either T_NAME or expression expected after K_FOR");
			return NULL;
		}
	}
	return NULL;
}

node_p parser_parse_def(parser_p v) {
	struct parser_t* p;
	
	p = v;
	if (match(p, K_DEF)) {
		if (match(p, T_NAME)) {
			node_p node;
			node_p block;
			
			node = node_new(N_DEF, "");
			node_add_child(node, node_new(N_NAME, get_matched_text()));
			if (match(p, T_LPAREN)) {
				node_add_child(node, parser_parse_def_params(p));
				if (match(p, T_RPAREN)) {
					/* fall through */
				} else {
					error(p, "T_RPAREN expected for K_DEF");
					node_free_recursively(node);
					return NULL;
				}
			}
			/* fall through here */
			if ((block = parser_parse_block(p))) {
				node_add_child(node, block);
				if (match(p, K_END)) {
					return node;
				} else {
					error(p, "K_END expected for K_DEF");
					node_free_recursively(node);
					return NULL;
				}
			} else {
				error(p, "block expected for K_DEF");
				node_free_recursively(node);
				return NULL;
			}
		} else {
			error(p, "T_NAME expected after K_DEF");
			return NULL;
		}
	}
	return NULL;
}

node_p parser_parse_def_params(parser_p v) {
	//
	return NULL;
}

node_p parser_parse_break(parser_p v) {
	struct parser_t* p;
	
	p = v;
	if (match(p, K_BREAK)) {
		return node_new(N_BREAK, "");
	}
	return NULL;
}

node_p parser_parse_continue(parser_p v) {
	struct parser_t* p;
	
	p = v;
	if (match(p, K_CONTINUE)) {
		return node_new(N_CONTINUE, "");
	}
	return NULL;
}

node_p parser_parse_return(parser_p v) {
	struct parser_t* p;
	
	p = v;
	if (match(p, K_RETURN)) {
		node_p node;
		node_p expression;
		
		node = node_new(N_RETURN, "");
		if ((expression = parser_parse_expression(p))) {
			node_add_child(node, expression);
		}
		return node;
	}
	return NULL;
}

node_p parser_parse_assignment(parser_p v) {
	//
	return NULL;
}

node_p parser_parse_expression(parser_p v) {
	//
	return NULL;
}
