#include <stdio.h>
#include "parser.h"
#include "node.h"

void print(node_p n, size_t indent) {
	size_t i;
	
	for (i = 0; i < indent; i++) {
		printf(" ");
	}
	printf("%d %s\n", node_get_type(n), node_get_text(n));
	for (i = 0; i < node_get_size(n); i++) {
		print(node_get_child(n, i), indent + 1);
	}
}

int main(int argc, char** argv) {
	parser_p p;
	node_p root;
	
	p = parser_new(stdin, stderr);
	if ((root = parser_parse_program(p))) {
		print(root, 0);
		node_free_recursively(root);
	}
	parser_free(p);
	return 0;
}