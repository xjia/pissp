#ifndef PISSP_NODE_H_
#define PISSP_NODE_H_

#include <stddef.h>

typedef void* node_p;

extern node_p node_new(int type, const char* text);
extern void node_free(node_p node);
extern void node_free_recursively(node_p node);
extern int node_get_type(node_p node);
extern const char* node_get_text(node_p node);
extern size_t node_get_size(node_p node);
extern node_p node_get_child(node_p node, size_t child_index);
extern void node_add_child(node_p node, node_p child);

#endif