Pissp Is Syntax Sugar for PHP
==============================

An introductory example
------------------------

In PHP:

```php
<html>
    <head>
        <title>Example</title>
    </head>
    <body>
        <?php
            echo "Hi, I'm a PHP script!";
        ?>
    </body>
</html>
```

In Pissp:

```erb
<html>
    <head>
        <title>Example</title>
    </head>
    <body>
        <%
            echo "Hi, I'm a PHP script!"
        %>
    </body>
</html>
```

or:

```erb
<html>
    <head>
        <title>Example</title>
    </head>
    <body>
        <%= "Hi, I'm a PHP script!" %>
    </body>
</html>
```

Printing a variable (Array element)
------------------------------------

```php
<?php
echo $_SERVER['HTTP_USER_AGENT'];
?>
```

```erb
<%
echo SERVER['HTTP_USER_AGENT']
%>
```

Example using control structures and functions
-----------------------------------------------

```php
<?php
if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
    echo 'You are using Internet Explorer.<br />';
}
?>
```

```erb
<%
if strpos(SERVER['HTTP_USER_AGENT'], 'MSIE') !== false
    echo 'You are using Internet Explorer.<br />'
end
%>
```

Mixing both HTML and PHP modes
-------------------------------

```php
<?php
if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
?>
    <h3>strpos() must have returned non-false</h3>
    <p>You are using Internet Explorer</p>
<?php
} else {
?>
    <h3>strpos() must have returned false</h3>
    <p>You are not using Internet Explorer</p>
<?php
}
?>
```

```erb
<% if strpos(SERVER['HTTP_USER_AGENT'], 'MSIE') !== false %>
    <h3>strpos() must have returned non-false</h3>
    <p>You are using Internet Explorer</p>
<% else %>
    <h3>strpos() must have returned false</h3>
    <p>You are not using Internet Explorer</p>
<% end %>
```

Printing data from our form
----------------------------

```php
Hi <?php echo htmlspecialchars($_POST['name']); ?>.
You are <?php echo (int)$_POST['age']; ?> years old.
```

```erb
Hi <%= htmlspecialchars POST['name'] %>.
You are <%= to_i POST['age'] %> years old.
```

Advanced escaping
------------------

```php
<?php
if ($expression) {
    ?>
    <strong>This is true.</strong>
    <?php
} else {
    ?>
    <strong>This is false.</strong>
    <?php
}
?>
```

```erb
<%
if expression
    %>
    <strong>This is true.</strong>
    <%
else
    %>
    <strong>This is false.</strong>
    <%
end
%>
```

```erb
<% if expression %>
    <strong>This is true.</strong>
<% else %>
    <strong>This is false.</strong>
<% end %>
```

Comments
---------

```php
<?php
    echo 'This is a test'; // This is a one-line c++ style comment
    /* This is a multi line comment
       yet another line of comment */
    echo 'This is yet another test';
    echo 'One Final Test'; # This is a one-line shell-style comment
?>
```

```erb
<%
    echo 'This is a test' // This is a one-line c++ style comment
    """ This is a multi line comment
        yet another line of comment """
    echo 'This is yet another test'
    echo 'One Final Test' # This is a one-line shell-style comment
%>
```

Types
------

```php
<?php
$a_bool = TRUE;   // a boolean
$a_str  = "foo";  // a string
$a_str2 = 'foo';  // a string
$an_int = 12;     // an integer

echo gettype($a_bool); // prints out:  boolean
echo gettype($a_str);  // prints out:  string

// If this is an integer, increment it by four
if (is_int($an_int)) {
    $an_int += 4;
}

// If $a_bool is a string, print it out
// (does not print out anything)
if (is_string($a_bool)) {
    echo "String: $a_bool";
}
?>
```

```erb
<%
a_bool = true   // a boolean
a_str  = "foo"  // a string
a_str2 = 'foo'  // a string
an_int = 12     // an integer

echo gettype a_bool // prints out:  boolean
echo gettype a_str  // prints out:  string

// If this is an integer, increment it by four
if is_int an_int
    an_int += 4
end

// If a_bool is a string, print it out
// (does not print out anything)
if is_string a_bool
    echo "String: #{a_bool}"
end
%>
```

Booleans
---------

```php
<?php
// == is an operator which tests
// equality and returns a boolean
if ($action == "show_version") {
    echo "The version is 1.23";
}

// this is not necessary...
if ($show_separators == TRUE) {
    echo "<hr>\n";
}

// ...because this can be used with exactly the same meaning:
if ($show_separators) {
    echo "<hr>\n";
}
?>
```

```erb
<%
// == is an operator which tests
// equality and returns a boolean
if action == "show_version"
    echo "The version is 1.23"
end

// this is not necessary...
if show_separators == true
    echo "<hr>\n"
end

// ...because this can be used with exactly the same meaning:
if show_separators
    echo "<hr>\n"
end
%>
```

Arrays
-------

In PHP:

```php
<?php
$arr = array("foo" => "bar", 12 => true);

echo $arr["foo"]; // bar
echo $arr[12];    // 1
?>
```

More:

```php
<?php
$arr = array("somearray" => array(6 => 5, 13 => 9, "a" => 42));

echo $arr["somearray"][6];    // 5
echo $arr["somearray"][13];   // 9
echo $arr["somearray"]["a"];  // 42
?>
```

However, Pissp doesn't allow such kinds of indexes. Instead,
Pissp makes use of JSON-like syntax and differentiates hashes
with arrays explicitly.

```erb
<%
arr = { "foo": "bar", 12: true }

echo arr["foo"]
echo arr["12"]
echo arr[12]    // this works because hash indexing will call to_s on the index
%>
```

```erb
<%
arr = { "somearray": { 6: 5, 13: 9, "a": 42 } }

echo arr["somearray"][6]
echo arr["somearray"][13]
echo arr["somearray"]["a"]
%>
```

Control structures
-------------------

```php
<?php
if ($a > $b) {
  echo "a is bigger than b";
  $b = $a;
}
?>
```

```erb
<%
if a > b
    echo "a is bigger than b"
    b = a
elif a == b
    echo "a is equal to b"
else
    echo "a is smaller than b"
end
%>
```

```php
<?php
$i = 1;
while ($i <= 10) {
    echo $i++;
}
?>
```

```erb
<%
i = 1
while i <= 10
    echo i++
end
%>
```

```php
<?php
do {
    if ($i < 5) {
        echo "i is not big enough";
        break;
    }
    $i *= $factor;
    if ($i < $minimum_limit) {
        break;
    }
    echo "i is ok";

    /* process i */

} while (0);
?>
```

```erb
<%
do_while 0
    if i < 5
        echo "i is not big enough"
        break
    end
    i *= factor
    if i < minimum_limit
        break
    end
    echo "i is ok"
    
    """ process i """
end
%>
```

```php
<?php
for ($i = 1; $i <= 10; $i += 2) {
    echo $i;
}
?>
```

```erb
<%
for i = 1 to 10 by 2
    echo i
end
%>
```

```php
<?php
$people = Array(
        Array('name' => 'Kalle', 'salt' => 856412),
        Array('name' => 'Pierre', 'salt' => 215863)
        );

for($i = 0; $i < sizeof($people); ++$i)
{
    $people[$i]['salt'] = rand(000000, 999999);
}
?>
```

```erb
<%
people = [
    { 'name': 'Kalle', 'salt': 856412 },
    { 'name': 'Pierre', 'salt': 215863 }
]

for i = 0 to sizeof(people)
    people[i]['salt'] = rand(000000, 999999)
end
%>
```

```php
<?php
$arr = array(1, 2, 3, 4);
foreach ($arr as &$value) {
    $value = $value * 2;
}
// $arr is now array(2, 4, 6, 8)
unset($value); // break the reference with the last element
?>
```

```erb
<%
arr = [1, 2, 3, 4]
for arr as value
    value = value * 2
end
// arr is now [2, 4, 6, 8]
unset value // break the reference with the last element
%>
```

```php
<?php
$a = array(1, 2, 3, 17);

foreach ($a as $v) {
    echo "Current value of \$a: $v.\n";
}
?>
```

```erb
<%
a = [1, 2, 3, 17]

for v in a
    echo "Current value of $a: #{v}.\n"
end
%>
```

```php
<?php
$a = array(
    "one" => 1,
    "two" => 2,
    "three" => 3,
    "seventeen" => 17
);

foreach ($a as $k => $v) {
    echo "\$a[$k] => $v.\n";
}
?>
```

```erb
<%
a = { "one": 1, "two": 2, "three": 3, "seventeen": 17 }

for a as k:v
    echo "$a[#{k}] => #{v}.\n"
end
%>
```

```php
<?php
$a = array();
$a[0][0] = "a";
$a[0][1] = "b";
$a[1][0] = "y";
$a[1][1] = "z";

foreach ($a as $v1) {
    foreach ($v1 as $v2) {
        echo "$v2\n";
    }
}
?>
```

```erb
<%
a = []
a[0][0] = "a"
a[0][1] = "b"
a[1][0] = "y"
a[1][1] = "z"

for v1 in a
    for v2 in v1
        echo "#{v2}\n"
    end
end
%>
```

```php
<?php
require "a.php";
include "b.php";
require_once "c.txt";
include_once "d.inc";
?>
```

```erb
<%
require "a"
include "b"
require_once "c.txt"
include_once "d.inc"
%>
```

Predefined Variables
---------------------

```php
<?php
$_SERVER
$_GET
$_POST
$_FILES
$_REQUEST
$_SESSION
$_ENV
$_COOKIE
?>
```

```erb
<%
SERVER
GET
POST
FILES
request
session
ENV
cookie
%>
```

And Pissp also provides <code>params</code> which contains
the contents of <code>GET</code> and <code>POST</code>.

Recursive functions
--------------------

```php
<?php
function recursion($a)
{
    if ($a < 20) {
        echo "$a\n";
        recursion($a + 1);
    }
}
?>
```

```erb
<%
def recursion(a)
    if a < 20
        echo "#{a}\n"
        recursion a+1
    end
end
%>
```

Functions within functions
---------------------------

```php
<?php
function foo() 
{
  function bar() 
  {
    echo "I don't exist until foo() is called.\n";
  }
}

/* We can't call bar() yet
   since it doesn't exist. */

foo();

/* Now we can call bar(),
   foo()'s processing has
   made it accessible. */

bar();

?>
```

```erb
<%
def foo
    def bar
        echo "I don't exist until foo() is called.\n"
    end
end

""" We can't call bar() yet
    since it doesn't exist. """

foo()

""" Now we can call bar(),
    foo()'s processing has
    made it accessible. """

bar()

%>
```

Passing function parameters by reference
-----------------------------------------

```php
<?php
function add_some_extra(&$string)
{
    $string .= 'and something extra.';
}
$str = 'This is a string, ';
add_some_extra($str);
echo $str;    // outputs 'This is a string, and something extra.'
?>
```

```erb
<%
def add_some_extra(&string)
    string .= 'and something extra.'
end
str = 'This is a string, '
add_some_extra str
echo str    // outputs 'This is a string, and something extra.'
%>
```

Returning values
-----------------

```php
<?php
function square($num)
{
    return $num * $num;
}
echo square(4);   // outputs '16'.
?>
```

```erb
<%
def square(num)
    return num * num
end
echo square(4)  // outputs '16'.
%>
```

In Pissp, the value of the last statement
in a function will be returned automatically.

```erb
<%
def square(num)
    num * num
end
echo square(4)  // outputs '16'.
%>
```

If a function takes only a single argument,
parenthesis can be omitted on calling.

```erb
<%
def print_square(num)
    echo num * num
end
square 4    // outputs '16'
%>
```

Variable function example
--------------------------

Function names on calling are assumed to be
declared publicly. If you want to use variable
functions, you have to prepend the dollar sign
explicitly and Pissp will keep it as it is.
Remember Pissp is just a syntax sugar. :-)

```php
<?php
function foo() {
    echo "In foo()<br />\n";
}

function bar($arg = '')
{
    echo "In bar(); argument was '$arg'.<br />\n";
}

// This is a wrapper function around echo
function echoit($string)
{
    echo $string;
}

$func = 'foo';
$func();        // This calls foo()

$func = 'bar';
$func('test');  // This calls bar()

$func = 'echoit';
$func('test');  // This calls echoit()
?>
```

```erb
<%
def foo
    echo "In foo()<br />\n"
end

def bar(arg = '')
    echo "In bar(); argument was '#{arg}'.<br />\n"
end

// This is a wrapper function around echo
def echoit(string)
    echo string
end

func = 'foo'
$func()         // This calls foo()

func = 'bar'
$func('test')   // This calls bar()

func = 'echoit'
$func('test')   // This calls echoit()
%>
```

Anonymous functions
--------------------

```php
<?php
echo preg_replace_callback('~-([a-z])~', function ($match) {
    return strtoupper($match[1]);
}, 'hello-world');
// outputs helloWorld
?>
```

```erb
<%
echo preg_replace_callback('~-([a-z])~',
        {|match| strtoupper(match[1]) },
        'hello-world')
%>
```

```php
<?php
$greet = function($name)
{
    printf("Hello %s\r\n", $name);
};

$greet('World');
$greet('PHP');
?>
```

```erb
<%
greet = {|name| printf("Hello %s\r\n", name) }

$greet('World')
$greet('PHP')
%>
```

Better if type-checking in introduced in Pissp
-----------------------------------------------

If type-checking is introduced in Pissp, most of the times
you do not have to prepend the dollar sign explicitly. For
example, in the following code snippet we can easily figure
out that <code>greet</code> is a function and we want to call
it.

```erb
<%
greet = {|name| printf("Hello %s\r\n", name) }
greet('World')
greet('PHP')
%>
```

And the following is different since is referencing a function
by name (not by value).

```erb
<%
def greet(name)
    printf("Hello %s\r\n", name)
end
// Keep the old style
func = 'greet'
$func('World')
$func('PHP')
%>
```

This feature needs further consideration and is considered to be
complicated to some extent, so I will not implement it right now.

Related Work
=============

* [PHP.rb](http://php.rubyforge.org/): A Ruby to PHP Code Generator

Authors
========

* [Xiao Jia](http://acm.sjtu.edu.cn/~xjia)

License
========

Pissp is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING or the accompanying LICENSE file
for more details.
